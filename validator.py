import os, sys, wx, datetime
from util import AddTmpUsrUtil as util

# ---------------------------------------------------------------------------------------------------------------------------------------
# Import file path libraries depending on the operating system; only linux is supported
# ---------------------------------------------------------------------------------------------------------------------------------------
osData = sys.platform.lower()
if osData.startswith('linux'):
    import pwd

class AddTmpUsrValidator(object):
    ErrorMsgs = \
    {
        'IsNonEmpty': '{name} cannot be empty.',
        'IsValidUmask': '{value} is not a valid umask. Each digit needs to be a number between 0 and 7.',
        'IsLengthRange': 'The length of {name} must be between {min} and {max} characters.',
        'IsLengthGt': 'The length of {name} must be at least {min} characters.',
        'IsLengthLt': 'The length of {name} must be at less than {max} characters.',
        'DoesParentDirExist': 'The parent directory of {value} from {name} does not exist.',
        'DoesFileExist': 'The path of {value} from {name} does not exist.',
        'DoesUserExist': 'The username, {value}, does not exist.',
        'IsUserUnique': 'The username, {value}, already exists.',
        'IsDateFuture': 'The {name} provided, {value}, is invalid. The date must be in the future.'
    }

    def __init__(self, name, obj, rules, usingGui = True):
        self._name = name
        self._obj = obj
        self._rules = rules.split("|")
        self._usingGui = usingGui

        if self._usingGui:
            self._currentBG = self._obj.GetBackgroundColour()
            self._currentFG = self._obj.GetForegroundColour()

        for rule in self._rules:
            assert (rule.split("(")[0] in AddTmpUsrValidator.ErrorMsgs), "Invalid rule"


    def ValidateAll(self):
        errors = []
        for rule in self._rules:
            msg = self.Validate(rule)
            if len(msg) > 0:
                errors.append(msg)

        if self._usingGui:
            if len(errors) > 0:
                # Indicate the invalid field
                self._obj.SetBackgroundColour('#ff3838')
                self._obj.SetForegroundColour('#ffffff')
            else:
                self._obj.SetBackgroundColour(self._currentBG)
                self._obj.SetForegroundColour(self._currentFG)

        return errors

    def Validate(self, rule):
        msg = eval('self.{rule}'.format(rule=rule))

        if self._usingGui:
            if len(msg) > 0:
                # Indicate the invalid field
                self._obj.SetBackgroundColour('#ff3838')
                self._obj.SetForegroundColour('#ffffff')
            else:
                self._obj.SetBackgroundColour(self._currentBG)
                self._obj.SetForegroundColour(self._currentFG)

        return msg

    def IsNonEmpty(self):
        if bool(self._obj.GetValue()):
            return ''
        else:
            return AddTmpUsrValidator.ErrorMsgs['IsNonEmpty'].format(name=self._name)


    def IsValidUmask(self):

        try:
            for char in list(self._obj.GetValue()):
                if int(char) < 0 or int(char) > 7:
                    return AddTmpUsrValidator.ErrorMsgs['IsValidUmask'].format(value=self._obj.GetValue())
        except ValueError:
            return AddTmpUsrValidator.ErrorMsgs['IsValidUmask'].format(value=self._obj.GetValue())

        return ''


    def IsLengthRange(self, min, max):
        var = self._obj.GetValue()

        if len(var) >= min and len(var) <= max:
            return ''
        else:
            return AddTmpUsrValidator.ErrorMsgs['IsLengthRange'].format(name=self._name, min=min, max=max)

    def IsLengthGt(self, min):
        var = self._obj.GetValue()

        if len(var) >= min:
            return ''
        else:
            return AddTmpUsrValidator.ErrorMsgs['IsLengthGt'].format(name=self._name, min=min)

    def IsLengthLt(self, min):
        var = self._obj.GetValue()

        if len(var) <= max:
            return ''
        else:
            return AddTmpUsrValidator.ErrorMsgs['IsLengthLt'].format(name=self._name, max=max)

    def DoesParentDirExist(self):
        filePath = self._obj.GetValue()

        try:
            if os.path.exists(os.path.dirname(filePath)):
                return ''
            else:
                return AddTmpUsrValidator.ErrorMsgs['DoesParentDirExist'].format(name=self._name, value=filePath)
        except:
            return AddTmpUsrValidator.ErrorMsgs['DoesParentDirExist'].format(name=self._name, value=filePath)

    def DoesFileExist(self):
        filePath = self._obj.GetValue()

        try:
            if os.path.exists(filePath):
                return ''
            else:
                return AddTmpUsrValidator.ErrorMsgs['DoesFileExist'].format(name=self._name, value=filePath)
        except:
            return AddTmpUsrValidator.ErrorMsgs['DoesFileExist'].format(name=self._name, value=filePath)

    def DoesUserExist(self):
        user = self._obj.GetValue()
        userExists = False

        if osData.startswith('linux'):
            try:
                pwd.getpwnam(user)
                userExists = True
            except KeyError:
                userExists = False

        if userExists:
            return ''
        else:
            return AddTmpUsrValidator.ErrorMsgs['DoesUserExist'].format(value=user)

    def IsUserUnique(self):
        user = self._obj.GetValue()
        userUnique = False

        if osData.startswith('linux'):
            try:
                pwd.getpwnam(user)
                userUnique = False
            except KeyError:
                userUnique = True

        if userUnique:
            return ''
        else:
            return AddTmpUsrValidator.ErrorMsgs['IsUserUnique'].format(value=user)

    def IsDateFuture(self):
        expDate = self._obj.GetValue()
        value = expDate.FormatISODate() if self._usingGui else expDate
        pyExpDate = util.Wxdate2Pydate(expDate) if self._usingGui else expDate
        nowDate = datetime.date.today()

        if pyExpDate > nowDate:
            return ''
        else:
            return AddTmpUsrValidator.ErrorMsgs['IsDateFuture'].format(name=self._name, value=value)


