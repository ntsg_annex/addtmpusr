# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Jun 17 2015)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
#import wx.xrc
import wx.richtext

###########################################################################
## Class MainFrame
###########################################################################

class MainFrame ( wx.Frame ):
	
	def __init__( self, parent ):
		wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = u"Add Temporary User", pos = wx.DefaultPosition, size = wx.Size( 769,592 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )
		
		self.SetSizeHintsSz( wx.Size( 733,560 ), wx.Size( -1,-1 ) )
		self.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )
		
		
		self.Centre( wx.BOTH )
		
		# Connect Events
		self.Bind( wx.EVT_CLOSE, self.OnClose )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def OnClose( self, event ):
		event.Skip()
	

###########################################################################
## Class CreateUserPanel
###########################################################################

class CreateUserPanel ( wx.Panel ):
	
	def __init__( self, parent ):
		wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( 740,567 ), style = wx.TAB_TRAVERSAL )
		
		self.SetBackgroundColour( wx.Colour( 255, 255, 255 ) )
		
		CreateUserVSizer = wx.BoxSizer( wx.VERTICAL )
		
		formGroupGSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		formGroupGSizer.AddGrowableCol( 1 )
		formGroupGSizer.SetFlexibleDirection( wx.BOTH )
		formGroupGSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.usernameLabel = wx.StaticText( self, wx.ID_ANY, u"Username", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.usernameLabel.Wrap( -1 )
		self.usernameLabel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		formGroupGSizer.Add( self.usernameLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		usernameHSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.usernameTextCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		usernameHSizer.Add( self.usernameTextCtrl, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.generateUsernameBtn = wx.Button( self, wx.ID_ANY, u"Generate Username", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.generateUsernameBtn.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		
		usernameHSizer.Add( self.generateUsernameBtn, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		formGroupGSizer.Add( usernameHSizer, 1, wx.EXPAND, 5 )
		
		self.groupsLabel = wx.StaticText( self, wx.ID_ANY, u"Groups", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.groupsLabel.Wrap( -1 )
		self.groupsLabel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		formGroupGSizer.Add( self.groupsLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		groupsHSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.extGroupCheckBox = wx.CheckBox( self, wx.ID_ANY, u"ext-sftp", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.extGroupCheckBox.SetValue(True) 
		self.extGroupCheckBox.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		self.extGroupCheckBox.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOW ) )
		
		groupsHSizer.Add( self.extGroupCheckBox, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.intextGroupCheckBox = wx.CheckBox( self, wx.ID_ANY, u"int-ext-sftp", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.intextGroupCheckBox.SetValue(True) 
		self.intextGroupCheckBox.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		groupsHSizer.Add( self.intextGroupCheckBox, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		
		formGroupGSizer.Add( groupsHSizer, 1, wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.expDateLabel = wx.StaticText( self, wx.ID_ANY, u"Expiration Date", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.expDateLabel.Wrap( -1 )
		self.expDateLabel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		formGroupGSizer.Add( self.expDateLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.expDatePickerCtrl = wx.DatePickerCtrl( self, wx.ID_ANY, wx.DefaultDateTime, wx.DefaultPosition, wx.DefaultSize, wx.DP_DEFAULT|wx.DP_DROPDOWN )
		formGroupGSizer.Add( self.expDatePickerCtrl, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.shellLabel = wx.StaticText( self, wx.ID_ANY, u"Shell", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.shellLabel.Wrap( -1 )
		self.shellLabel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		formGroupGSizer.Add( self.shellLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.shellTextCtrl = wx.TextCtrl( self, wx.ID_ANY, u"/sbin/nologin", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.shellTextCtrl.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		
		formGroupGSizer.Add( self.shellTextCtrl, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.homeDirLabel = wx.StaticText( self, wx.ID_ANY, u"Home Directory", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.homeDirLabel.Wrap( -1 )
		self.homeDirLabel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		formGroupGSizer.Add( self.homeDirLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.homeDirTextCtrl = wx.TextCtrl( self, wx.ID_ANY, u"/ftp/ext", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.homeDirTextCtrl.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		
		formGroupGSizer.Add( self.homeDirTextCtrl, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.umaskLabel = wx.StaticText( self, wx.ID_ANY, u"Umask", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.umaskLabel.Wrap( -1 )
		self.umaskLabel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		formGroupGSizer.Add( self.umaskLabel, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.umaskTextCtrl = wx.TextCtrl( self, wx.ID_ANY, u"022", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.umaskTextCtrl.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		
		formGroupGSizer.Add( self.umaskTextCtrl, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.passwordLabel = wx.StaticText( self, wx.ID_ANY, u"Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.passwordLabel.Wrap( -1 )
		self.passwordLabel.SetForegroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_WINDOWTEXT ) )
		
		formGroupGSizer.Add( self.passwordLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		passwordHSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.passwordTextCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		passwordHSizer.Add( self.passwordTextCtrl, 10, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		passwordGenHSizer = wx.BoxSizer( wx.HORIZONTAL )
		
		self.lowercaseSpinCtrl = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 3 )
		self.lowercaseSpinCtrl.SetToolTipString( u"No. of lowercase alpha characters" )
		self.lowercaseSpinCtrl.SetMinSize( wx.Size( 50,-1 ) )
		self.lowercaseSpinCtrl.SetMaxSize( wx.Size( 50,-1 ) )
		
		passwordGenHSizer.Add( self.lowercaseSpinCtrl, 0, wx.ALL, 5 )
		
		self.uppercaseSpinCtrl = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 3 )
		self.uppercaseSpinCtrl.SetToolTipString( u"No. of uppercase alpha characters" )
		self.uppercaseSpinCtrl.SetMinSize( wx.Size( 50,-1 ) )
		self.uppercaseSpinCtrl.SetMaxSize( wx.Size( 50,-1 ) )
		
		passwordGenHSizer.Add( self.uppercaseSpinCtrl, 0, wx.ALL, 5 )
		
		self.digitsSpinCtrl = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 3 )
		self.digitsSpinCtrl.SetToolTipString( u"No. of digit characters" )
		self.digitsSpinCtrl.SetMinSize( wx.Size( 50,-1 ) )
		self.digitsSpinCtrl.SetMaxSize( wx.Size( 50,-1 ) )
		
		passwordGenHSizer.Add( self.digitsSpinCtrl, 0, wx.ALL, 5 )
		
		self.symbolSpinCtrl = wx.SpinCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.SP_ARROW_KEYS, 0, 10, 1 )
		self.symbolSpinCtrl.SetToolTipString( u"No. of symbol characters" )
		self.symbolSpinCtrl.SetMinSize( wx.Size( 50,-1 ) )
		self.symbolSpinCtrl.SetMaxSize( wx.Size( 50,-1 ) )
		
		passwordGenHSizer.Add( self.symbolSpinCtrl, 0, wx.ALL, 5 )
		
		
		passwordHSizer.Add( passwordGenHSizer, 0, wx.EXPAND, 5 )
		
		self.generatePasswordBtn = wx.Button( self, wx.ID_ANY, u"Generate Password", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.generatePasswordBtn.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		
		passwordHSizer.Add( self.generatePasswordBtn, 5, wx.ALL|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		
		formGroupGSizer.Add( passwordHSizer, 1, wx.EXPAND, 5 )
		
		self.notesLabel = wx.StaticText( self, wx.ID_ANY, u"Notes", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.notesLabel.Wrap( -1 )
		formGroupGSizer.Add( self.notesLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.notesTextCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
		self.notesTextCtrl.SetMaxLength( 120 ) 
		formGroupGSizer.Add( self.notesTextCtrl, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		formGroupGSizer.AddSpacer( ( 0, 0), 1, wx.EXPAND, 5 )
		
		self.createUserBtn = wx.Button( self, wx.ID_ANY, u"Create User", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.createUserBtn.SetFont( wx.Font( wx.NORMAL_FONT.GetPointSize(), 70, 90, 90, False, wx.EmptyString ) )
		self.createUserBtn.SetForegroundColour( wx.Colour( 0, 0, 0 ) )
		self.createUserBtn.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		
		formGroupGSizer.Add( self.createUserBtn, 1, wx.ALL|wx.ALIGN_CENTER_HORIZONTAL|wx.EXPAND, 5 )
		
		
		CreateUserVSizer.Add( formGroupGSizer, 1, wx.EXPAND, 5 )
		
		self.outputRichTextCtrl = wx.richtext.RichTextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY|wx.HSCROLL|wx.NO_BORDER|wx.VSCROLL|wx.WANTS_CHARS )
		self.outputRichTextCtrl.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_BTNFACE ) )
		
		CreateUserVSizer.Add( self.outputRichTextCtrl, 1, wx.EXPAND|wx.ALL|wx.ALIGN_CENTER_HORIZONTAL, 5 )
		
		filePathsGSizer = wx.FlexGridSizer( 0, 2, 0, 0 )
		filePathsGSizer.AddGrowableCol( 1 )
		filePathsGSizer.SetFlexibleDirection( wx.BOTH )
		filePathsGSizer.SetNonFlexibleGrowMode( wx.FLEX_GROWMODE_SPECIFIED )
		
		self.userOutputLabel = wx.StaticText( self, wx.ID_ANY, u"User Output File Path", wx.DefaultPosition, wx.DefaultSize, wx.ALIGN_LEFT )
		self.userOutputLabel.Wrap( -1 )
		filePathsGSizer.Add( self.userOutputLabel, 0, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.ALIGN_RIGHT, 5 )
		
		self.userOutputTextCtrl = wx.TextCtrl( self, wx.ID_ANY, u"/root/tmp_users/", wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.userOutputTextCtrl.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		
		filePathsGSizer.Add( self.userOutputTextCtrl, 1, wx.ALL|wx.ALIGN_CENTER_VERTICAL|wx.EXPAND, 5 )
		
		self.configFileLabel = wx.StaticText( self, wx.ID_ANY, u"Config File Path", wx.DefaultPosition, wx.DefaultSize, 0 )
		self.configFileLabel.Wrap( -1 )
		filePathsGSizer.Add( self.configFileLabel, 0, wx.ALL|wx.ALIGN_RIGHT|wx.ALIGN_CENTER_VERTICAL, 5 )
		
		self.configFileTextCtrl = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, wx.TE_READONLY )
		self.configFileTextCtrl.SetBackgroundColour( wx.SystemSettings.GetColour( wx.SYS_COLOUR_3DLIGHT ) )
		
		filePathsGSizer.Add( self.configFileTextCtrl, 1, wx.ALL|wx.EXPAND, 5 )
		
		
		CreateUserVSizer.Add( filePathsGSizer, 0, wx.EXPAND, 5 )
		
		
		self.SetSizer( CreateUserVSizer )
		self.Layout()
		
		# Connect Events
		self.generateUsernameBtn.Bind( wx.EVT_BUTTON, self.GenerateUsername )
		self.generatePasswordBtn.Bind( wx.EVT_BUTTON, self.GeneratePassword )
		self.createUserBtn.Bind( wx.EVT_BUTTON, self.CreateUser )
	
	def __del__( self ):
		pass
	
	
	# Virtual event handlers, overide them in your derived class
	def GenerateUsername( self, event ):
		event.Skip()
	
	def GeneratePassword( self, event ):
		event.Skip()
	
	def CreateUser( self, event ):
		event.Skip()
	

