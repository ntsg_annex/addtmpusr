from argparse import ArgumentParser, ArgumentTypeError
from datetime import datetime
from console import ConsoleObj

class Arguments(object):
    """Uses ArgumentParser to gather and parse arguments given by the user in the command line.

    Attributes:
        isSilent (bool): Whether or not we're creating a user silently
        genUser
        genPass
        username
        groups
        password
        notes
    """
    def __init__(self):
        # Update the current time
        self.currTime = datetime.now().strftime("%b-%d-%Y_%H-%M-%S")

        # Setup arguments to parse
        parser = ArgumentParser(description='Adds temporary users and outputs the new user information to a JSON file.')

        # Have the program do different things depending on the command needed
        subParsers = parser.add_subparsers(help='ADDTMPUSR SUBCOMMANDS.', dest='command')
        genconfParser = subParsers.add_parser('genconf', help='Generates the config file needed to customize temporary user creation.')
        genpassParser = subParsers.add_parser('genpass', help='Generates a password.')
        genuserParser = subParsers.add_parser('genuser', help='Generates a username from the JSON config file.')
        guiParser = subParsers.add_parser('gui', help='Opens a GUI to generate a user.')
        silentParser = subParsers.add_parser('silent', help='Uses command line arguments to create a user via the command line.')

        # Generate config parser
        genconfParser.add_argument('-v', '--verbose', action='store_true', help='Whether or not to log verbose messages and datetime stamps. Errors will still be logged.')

        # Generate pass positional arguments
        genpassParser.add_argument('-l', '--lower', type=int, default=3, help='The number of lowercase alpha characters.')
        genpassParser.add_argument('-u', '--upper', type=int, default=3, help='The number of uppercase alpha characters.')
        genpassParser.add_argument('-d', '--digit', type=int, default=3, help='The number of digits.')
        genpassParser.add_argument('-s', '--symbol', type=int, default=1, help='The number of symbols.')
        genpassParser.add_argument('-v', '--verbose', action='store_true', help='Whether or not to log verbose messages and datetime stamps. Errors will still be logged.')

        # Generate user positional arguments
        genuserParser.add_argument('-p', '--prefix', default='tmp_', help='The prefix before the username generated. Default is "tmp_".')
        genuserParser.add_argument('-v', '--verbose', action='store_true', help='Whether or not to log verbose messages and datetime stamps. Errors will still be logged.')

        # Gui parser
        # Doesn't have anything to process

        # Silent parser positional arguments
        silentParser.add_argument('-u', '--username', required=True, help='The username of the new user. It must be unique.')
        silentParser.add_argument('-g', '--groups', choices=['"ext-sftp"','"int-ext-sftp"','"int-ext-sftp,ext-sftp"'], default='"int-ext-sftp,ext-sftp"',
                                  help='A comma-delimited list of groups that will be added to the user. Default is "int-ext-sftp,ext-sftp".')
        silentParser.add_argument('-x', '--expdate', required=True, type=self.valid_date, help='The expiration date of the account when it will be disabled. It must be in the future.')
        silentParser.add_argument('-p', '--pass', required=True, help='The temporary password. It must be at least 7 characters long.')
        silentParser.add_argument('-n', '--notes', required=True, help='The notes associated with the temporary user.')
        silentParser.add_argument('-v', '--verbose', action='store_true', help='Whether or not to log verbose messages and datetime stamps. Errors will still be logged.')

        self._args = vars(parser.parse_args())

    def valid_date(self, s):
        try:
            return datetime.strptime(s, "%Y-%m-%d").date()
        except ValueError:
            msg = "[ERROR]: Not a valid date. Expecting: YYYY-mm-dd, not '{0}'.".format(s)
            raise ArgumentTypeError(msg)

    @property
    def args(self):
        return self._args







