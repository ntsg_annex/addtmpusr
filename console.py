from config import AddTmpUsrAppConfig as conf
from util import AddTmpUsrUtil as util
from validator import AddTmpUsrValidator as Validator
from datetime import datetime
import sys, subprocess, json, os

# ---------------------------------------------------------------------------------------------------------------------------------------
# Import file path libraries depending on the operating system; only linux is supported
# ---------------------------------------------------------------------------------------------------------------------------------------
osData = sys.platform.lower()
if osData.startswith('linux'):
    import crypt
else:
    pass
    #raise RuntimeError('The current operating system is not supported.')

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class SilentConsole:
    def __init__(self, args):
        self._verbose = args['verbose']
        self._username = ConsoleObj('Username', args['username'], rules='IsNonEmpty()|IsUserUnique()')
        self._expDate = ConsoleObj('Expiration Date', args['expdate'], rules='IsNonEmpty()|IsDateFuture()')
        self._password = ConsoleObj('Password', args['pass'], rules='IsNonEmpty()|IsLengthGt(7)')
        self._notes = ConsoleObj('Notes', args['notes'], rules='IsNonEmpty()|IsLengthLt(121)')
        self._shell = ConsoleObj('Shell Path', '', rules='IsNonEmpty()|DoesFileExist()')
        self._umask = ConsoleObj('Umask', '', rules='IsNonEmpty()|IsLengthRange(3,4)|IsValidUmask()')
        self._homeDir = ConsoleObj('Home Directory', '', rules='IsNonEmpty()|DoesFileExist()')
        self._userOutput = ConsoleObj('User Output Path', '', rules='IsNonEmpty()|DoesFileExist()')
        # This is chosen from a list of acceptable groups, so this does not need to be validated and thus doesn't need to be in a console object
        self._groups = args['groups']

        # Update config controls
        self.SetConfigCtrls()

        # Create validators
        self.SetValidators()

    def SetConfigCtrls(self):
        # Retrieve the user output path from the user config. By default, the tmp_users folder is in the config folder
        userOutputPath = conf.Instance().GetConfigValue('useroutput')
        if userOutputPath is None:
            userOutputDir = conf.Instance().GetConfigFile('tmp_users')
            conf.Instance().WriteConfigValue('useroutput', userOutputDir)
        self._userOutput.SetValue(str(conf.Instance().GetConfigValue('useroutput', userOutputPath)))

        # Retrieve umask from user config
        umask = conf.Instance().GetConfigValue('umask')
        if umask is None:
            conf.Instance().WriteConfigValue('umask', '022')
        self._umask.SetValue(str(conf.Instance().GetConfigValue('umask', umask)))

        # Retrieve shell from the user config
        shell = conf.Instance().GetConfigValue('shell')
        if shell is None:
            conf.Instance().WriteConfigValue('shell', '/sbin/nologin')
        self._shell.SetValue(str(conf.Instance().GetConfigValue('shell', shell)))

        # Retrieve home dir from the user config
        homeDir = conf.Instance().GetConfigValue('home')
        if homeDir is None:
            conf.Instance().WriteConfigValue('home', '/ftp/ext')
        self._homeDir.SetValue(str(conf.Instance().GetConfigValue('home', homeDir)))

    def SetValidators(self):
        self.usernameValidator = Validator(name=self._username.GetName(), obj=self._username, rules=self._username.GetRules(), usingGui = False)
        self.expDateValidator = Validator(name=self._expDate.GetName(), obj=self._expDate, rules=self._expDate.GetRules(), usingGui = False)
        self.shellValidator = Validator(name=self._shell.GetName(), obj=self._shell, rules=self._shell.GetRules(), usingGui = False)
        self.homeDirValidator = Validator(name=self._homeDir.GetName(), obj=self._homeDir, rules=self._homeDir.GetRules(), usingGui = False)
        self.umaskValidator = Validator(name=self._umask.GetName(), obj=self._umask, rules=self._umask.GetRules(), usingGui = False)
        self.passwordValidator = Validator(name=self._password.GetName(), obj=self._password, rules=self._password.GetRules(), usingGui = False)
        self.notesValidator = Validator(name=self._notes.GetName(), obj=self._notes, rules=self._notes.GetRules(), usingGui = False)
        self.userOutputValidator = Validator(name=self._userOutput.GetName(), obj=self._userOutput, rules=self._userOutput.GetRules(), usingGui = False)

    def CreateUser(self):
        # Validate input
        errorMsgs = []
        errorCount = 0
        errorMsgs.append(self.usernameValidator.ValidateAll())
        errorMsgs.append(self.expDateValidator.ValidateAll())
        errorMsgs.append(self.shellValidator.ValidateAll())
        errorMsgs.append(self.homeDirValidator.ValidateAll())
        errorMsgs.append(self.umaskValidator.ValidateAll())
        errorMsgs.append(self.passwordValidator.ValidateAll())
        errorMsgs.append(self.notesValidator.ValidateAll())
        errorMsgs.append(self.userOutputValidator.ValidateAll())
        for msg in errorMsgs:
            if len(msg) > 0:
                LoggingConsole.LogError(msg)
                errorCount = errorCount + 1

        # Continue on if validation was successful
        if errorCount == 0:
            # Get all values
            data = \
            {
                'username': self._username.GetValue(),
                'password': self._password.GetValue(),
                'expDate': self._expDate.GetValue().isoformat(),
                'shell': self._shell.GetValue(),
                'homeDir': self._homeDir.GetValue(),
                'umask': self._umask.GetValue(),
                'notes': self._notes.GetValue()
            }

            encPass = crypt.crypt(self._password.GetValue(), "22")
            userOutput = self._userOutput.GetValue()
            conf.Instance().WriteConfigValue('useroutput', userOutput)

            command = 'useradd -p {encPass} -d {homeDir} -e {expDate} -G {groups} -s {shell} -K UMASK={umask} {username}'.format \
            (
                username=data['username'],
                encPass=encPass,
                homeDir=data['homeDir'],
                expDate=data['expDate'],
                groups=self._groups,
                shell=data['shell'],
                umask=data['umask']
            )

            try:
                useraddOutput = subprocess.check_output(command, shell=True)
                if self._verbose:
                    if (len(useraddOutput) > 0):
                        LoggingConsole.LogVerbose(useraddOutput)
                    LoggingConsole.LogSuccess('User successfully created.')

                userOutputFilePath = os.path.join(userOutput, '{username}.json'.format(username=data['username']))
                with open(userOutputFilePath, 'w') as outFile:
                    json.dump(data, outFile, indent=4)

                if self._verbose:
                    LoggingConsole.LogSuccess('File written to {filePath}.'.format(filePath=userOutputFilePath))

            except subprocess.CalledProcessError as ex:
                LoggingConsole.LogError(ex.message)
            except Exception as ex:
                LoggingConsole.LogError(ex.message)

        if self._verbose:
            LoggingConsole.LogVerbose('========================================================')


class ConsoleObj:
    def __init__(self, name, value, rules):
        self._name = name
        self._value = value
        self._rules = rules

    def SetValue(self, newval):
        self._value = newval

    def SetName(self, newname):
        self._name = newname

    def SetRules(self, newrules):
        self._rules = newrules

    def GetValue(self):
        return self._value

    def GetName(self):
        return self._name

    def GetRules(self):
        return self._rules


class GenUserConsole:
    def __init__(self, args):
        self._prefix = args['prefix']
        self._verbose = args['verbose']

    def CreateUser(self):
        # Retrieve an array of possible usernames from the config file (if it exists)
        arr = conf.Instance().GetConfigValue("usernames")
        username = ""
        if arr is not None:
            username = util.CreateUsername(prefix=self._prefix, arr=arr)
            if self._verbose:
                LoggingConsole.LogSuccess("Username Generated: {username}".format(username=username))
            else:
                print(username)
        else:
            LoggingConsole.LogError("Usernames cannot be generated because the 'usernames' array in the JSON config file is not defined.")

class GenPassConsole:
    def __init__(self, args):
        self._lower = args['lower']
        self._upper = args['upper']
        self._digits = args['digit']
        self._symbols = args['symbol']
        self._verbose = args['verbose']

    def CreatePass(self):
        password = util.CreatePassword(self._upper, self._lower, self._digits, self._symbols)
        if self._verbose:
            LoggingConsole.LogSuccess("Password Generated: {password}".format(password=password))
        else:
            print(password)

class GenConfConsole:
    def __init__(self, args):
        self._verbose = args['verbose']

    def CreateConf(self):
        conf.Instance()

        # Retrieve the user output path from the user config. By default, the tmp_users folder is in the config folder
        userOutputPath = conf.Instance().GetConfigValue('useroutput')
        if userOutputPath is None:
            userOutputDir = conf.Instance().GetConfigFile('tmp_users')
            conf.Instance().WriteConfigValue('useroutput', userOutputDir)

        # Retrieve umask from user config
        umask = conf.Instance().GetConfigValue('umask')
        if umask is None:
            conf.Instance().WriteConfigValue('umask', '022')

        # Retrieve shell from the user config
        shell = conf.Instance().GetConfigValue('shell')
        if shell is None:
            conf.Instance().WriteConfigValue('shell', '/sbin/nologin')

        # Retrieve home dir from the user config
        homeDir = conf.Instance().GetConfigValue('home')
        if homeDir is None:
            conf.Instance().WriteConfigValue('home', '/ftp/ext')

        if self._verbose:
            LoggingConsole.LogSuccess("Config created in: {confPath}".format( confPath=conf.Instance().ConfigFile ))

class LoggingConsole(object):

    @staticmethod
    def LogError(msg):
        print("[{datetime}][ERROR]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')),message=msg))

    @staticmethod
    def LogSuccess(msg):
        print("[{datetime}][SUCCESS]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg))

    @staticmethod
    def LogWarning(msg):
        print("[{datetime}][WARNING]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg))

    @staticmethod
    def LogVerbose(msg):
        print("[{datetime}][VERBOSE]: {message}".format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')), message=msg))


