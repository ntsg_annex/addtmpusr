import string, random, datetime, wx

class AddTmpUsrUtil(object):

    @staticmethod
    def CreateUsername(arr, prefix="tmp_"):
        return "{prefix}{username}".format(prefix=prefix,username=random.choice(arr))

    @staticmethod
    def CreatePassword(upper, lower, digits, symbols):
        charTypes = [upper, lower, digits, symbols]
        password = ''

        # Create a password with the specified number of upper, lower, digits, and symbols
        for charType in range(len(charTypes)):
            for char in range(int( charTypes[charType] )):
                if charType == 0:
                    password += string.uppercase[ random.randint(0, len(string.uppercase) - 1 )]
                elif charType == 1:
                    password += string.lowercase[ random.randint(0, len(string.lowercase) - 1) ]
                elif charType == 2:
                    password += string.digits[ random.randint(0, len(string.digits) - 1) ]
                elif charType == 3:
                    password += string.punctuation[ random.randint(0, len(string.punctuation) - 1) ]

        # Shuffle the characters in the password so it isn't always upper, lower, digits, and symbols
        password = ''.join(random.sample(password, len(password)))

        return password

    @staticmethod
    def Pydate2Wxdate(date):
        assert isinstance(date, (datetime.datetime, datetime.date))
        tt = date.timetuple()
        dmy = (tt[2], tt[1] - 1, tt[0])
        return wx.DateTimeFromDMY(*dmy)

    @staticmethod
    def Wxdate2Pydate(date):
        assert isinstance(date, wx.DateTime)
        if date.IsValid():
            ymd = map(int, date.FormatISODate().split('-'))
            return datetime.date(*ymd)
        else:
            return None



