from config import AddTmpUsrAppConfig as conf
from util import AddTmpUsrUtil as util
from validator import AddTmpUsrValidator as Validator
from datetime import datetime
import sys, wx, wxfbgui, os, json, subprocess

# ---------------------------------------------------------------------------------------------------------------------------------------
# Import file path libraries depending on the operating system; only linux is supported
# ---------------------------------------------------------------------------------------------------------------------------------------
osData = sys.platform.lower()
if osData.startswith('linux'):
    import crypt
else:
    pass
    #raise RuntimeError('The current operating system is not supported.')

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# USER INTERFACE
# ---------------------------------------------------------------------------------------------------------------------------------------


class CreateUserPanel(wxfbgui.CreateUserPanel):
    def __init__(self, parent):
        super(CreateUserPanel, self).__init__(parent)

        # Update tvalues read from config files
        self.SetConfigCtrls()

        # Set validators
        self.SetValidators()

        # Greet user
        self.LogVerbose("""
        Welcome to the 'add temporary user' program!
        All read-only text fields can be modified using the JSON config file (except for the config file location).
        To run this program, you either must be root or run it as sudo.
        """)
        self.LogVerbose("========================================================")

    def SetValidators(self):

        self.usernameValidator = Validator(name='Username', obj=self.usernameTextCtrl, rules='IsNonEmpty()|IsUserUnique()')
        self.expDateValidator = Validator(name='Expiration Date', obj=self.expDatePickerCtrl, rules='IsNonEmpty()|IsDateFuture()')
        self.shellValidator = Validator(name='Shell Path', obj=self.shellTextCtrl, rules='IsNonEmpty()|DoesFileExist()')
        self.homeDirValidator = Validator(name='Home Directory', obj=self.homeDirTextCtrl, rules='IsNonEmpty()|DoesFileExist()')
        self.umaskValidator = Validator(name='Umask', obj=self.umaskTextCtrl, rules='IsNonEmpty()|IsLengthRange(3,4)|IsValidUmask()')
        self.passwordValidator = Validator(name='Password', obj=self.passwordTextCtrl, rules='IsNonEmpty()|IsLengthGt(7)')
        self.notesValidator = Validator(name='Notes', obj=self.notesTextCtrl, rules='IsNonEmpty()|IsLengthLt(121)')
        self.userOutputValidator = Validator(name='User Output Path', obj=self.userOutputTextCtrl, rules='IsNonEmpty()|DoesFileExist()')


    def SetConfigCtrls(self):
        # Retrieve the user output path from the user config. By default, the tmp_users folder is in the config folder
        userOutputPath = conf.Instance().GetConfigValue('useroutput')
        if userOutputPath is None:
            userOutputDir = conf.Instance().GetConfigFile('tmp_users')
            conf.Instance().WriteConfigValue('useroutput', userOutputDir)
        self.userOutputTextCtrl.SetValue(str(conf.Instance().GetConfigValue('useroutput', userOutputPath)))

        # Set config file path
        self.configFileTextCtrl.SetValue(str(conf.Instance().ConfigFile))

        # Retrieve umask from user config
        umask = conf.Instance().GetConfigValue('umask')
        if umask is None:
            conf.Instance().WriteConfigValue('umask', '022')
        self.umaskTextCtrl.SetValue(str(conf.Instance().GetConfigValue('umask', umask)))

        # Retrieve shell from the user config
        shell = conf.Instance().GetConfigValue('shell')
        if shell is None:
            conf.Instance().WriteConfigValue('shell', '/sbin/nologin')
        self.shellTextCtrl.SetValue(str(conf.Instance().GetConfigValue('shell', shell)))

        # Retrieve home dir from the user config
        homeDir = conf.Instance().GetConfigValue('home')
        if homeDir is None:
            conf.Instance().WriteConfigValue('home', '/ftp/ext')
        self.homeDirTextCtrl.SetValue(str(conf.Instance().GetConfigValue('home', homeDir)))

    def CreateUser(self, event):

        # Validate form
        errorMsgs = []
        errorCount = 0
        errorMsgs.append(self.usernameValidator.ValidateAll())
        errorMsgs.append(self.expDateValidator.ValidateAll())
        errorMsgs.append(self.shellValidator.ValidateAll())
        errorMsgs.append(self.homeDirValidator.ValidateAll())
        errorMsgs.append(self.umaskValidator.ValidateAll())
        errorMsgs.append(self.passwordValidator.ValidateAll())
        errorMsgs.append(self.notesValidator.ValidateAll())
        errorMsgs.append(self.userOutputValidator.ValidateAll())
        for msg in errorMsgs:
            if len(msg) > 0:
                self.LogError(msg)
                errorCount = errorCount + 1

        # Continue on if validation was successful
        if errorCount == 0:
            # Get an array of groups checked
            groups = []
            if self.extGroupCheckBox.IsChecked():
                groups.append('ext-sftp')
            if self.intextGroupCheckBox.IsChecked():
                groups.append('int-ext-sftp')
            groupStr = ','.join(groups)

            # Get all values
            data =\
            {
                'username': self.usernameTextCtrl.GetValue(),
                'password': self.passwordTextCtrl.GetValue(),
                'expDate': self.expDatePickerCtrl.GetValue().FormatISODate(),
                'shell': self.shellTextCtrl.GetValue(),
                'homeDir': self.homeDirTextCtrl.GetValue(),
                'umask': self.umaskTextCtrl.GetValue(),
                'notes': self.notesTextCtrl.GetValue()
            }

            encPass = crypt.crypt(self.passwordTextCtrl.GetValue(), "22")
            userOutput = self.userOutputTextCtrl.GetValue()
            conf.Instance().WriteConfigValue('useroutput', userOutput)

            command = 'useradd -p {encPass} -d {homeDir} -e {expDate} -G {groups} -s {shell} -K UMASK={umask} {username}'.format\
            (
                username=data['username'],
                encPass=encPass,
                homeDir=data['homeDir'],
                expDate=data['expDate'],
                groups=groupStr,
                shell=data['shell'],
                umask=data['umask']
            )

            try:
                useraddOutput = subprocess.check_output(command, shell=True)
                if(len(useraddOutput) > 0):
                    self.LogVerbose(useraddOutput)
                self.LogSuccess('User successfully created.')

                userOutputFilePath = os.path.join(userOutput, '{username}.json'.format(username=data['username']))
                with open(userOutputFilePath, 'w') as outFile:
                    json.dump(data, outFile, indent=4)
                self.LogSuccess('File written to {filePath}.'.format(filePath=userOutputFilePath))

            except subprocess.CalledProcessError as ex:
                self.LogError(ex.message)
            except Exception as ex:
                self.LogError(ex.message)

        self.LogVerbose('========================================================')

        self.Refresh()

    def GeneratePassword(self, event):
        # Get the values from the spin controls and use them to generate a password
        upper = self.uppercaseSpinCtrl.GetValue()
        lower = self.lowercaseSpinCtrl.GetValue()
        digits = self.digitsSpinCtrl.GetValue()
        symbols = self.symbolSpinCtrl.GetValue()
        password = util.CreatePassword(upper, lower, digits, symbols)

        self.passwordTextCtrl.SetValue(str(password))

    def GenerateUsername(self, event):
        # Retrieve an array of possible usernames from the config file (if it exists)
        arr = conf.Instance().GetConfigValue("usernames")
        username = ""
        if arr is not None:
            username = util.CreateUsername(arr)
        else:
            self.LogError("Usernames cannot be generated because the 'usernames' array in the config file is not defined.")

        self.usernameTextCtrl.SetValue(str(username))

    def LogError(self, msg):
        self.outputRichTextCtrl.BeginTextColour((244, 66, 66)) # Red text
        self.outputRichTextCtrl.WriteText("[{datetime}][ERROR]: {message}"
                                          .format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')),message=msg))
        self.outputRichTextCtrl.Newline()
        self.outputRichTextCtrl.EndTextColour()

        # Scroll to the bottom
        self.outputRichTextCtrl.ScrollLines(self.outputRichTextCtrl.GetScrollRange(0))

    def LogSuccess(self, msg):
        self.outputRichTextCtrl.BeginTextColour((43, 165, 67)) # Green text
        self.outputRichTextCtrl.WriteText("[{datetime}][SUCCESS]: {message}"
                                          .format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')),message=msg))
        self.outputRichTextCtrl.Newline()
        self.outputRichTextCtrl.EndTextColour()

        # Scroll to the bottom
        self.outputRichTextCtrl.ScrollLines(self.outputRichTextCtrl.GetScrollRange(0))

    def LogWarning(self, msg):
        self.outputRichTextCtrl.BeginTextColour((244, 152, 66)) # Orange text
        self.outputRichTextCtrl.WriteText("[{datetime}][WARNING]: {message}"
                                          .format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')),message=msg))
        self.outputRichTextCtrl.Newline()
        self.outputRichTextCtrl.EndTextColour()

        # Scroll to the bottom
        self.outputRichTextCtrl.ScrollLines(self.outputRichTextCtrl.GetScrollRange(0))

    def LogVerbose(self, msg):
        self.outputRichTextCtrl.BeginTextColour((0, 0, 0)) # Black text
        self.outputRichTextCtrl.WriteText("[{datetime}][VERBOSE]: {message}"
                                          .format(datetime=str(datetime.now().strftime('%Y/%m/%d %H:%M:%S')),message=msg))
        self.outputRichTextCtrl.Newline()
        self.outputRichTextCtrl.EndTextColour()

        self.outputRichTextCtrl.ScrollLines(self.outputRichTextCtrl.GetScrollRange(0))

class AddTmpUsrFrame(wxfbgui.MainFrame):
    def __init__(self, parent):
        super(AddTmpUsrFrame, self).__init__(parent)
        self.panel = CreateUserPanel(self)

    def OnClose(self, event):
        self.Destroy()

class AddTmpUsrApp(wx.App):
    def OnInit(self):
        self.frame = AddTmpUsrFrame(None)
        self.frame.Show()
        return True

    @property
    def AppName(self):
        return "addtmpusr"

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////