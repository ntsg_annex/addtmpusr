# addtmpusr

## Dependencies

- Linux Only
- Python >= 2.7
- WxPython 3.0


## Overview

This python program aides in the management of temporary users. Although these temporary users can have many purposes, the main purpose for us is to limit the window in which external users can access our FTP filesystem, `/ftp/ext`, via SFTP.

There are five commands that can be used with this progam:

- `genconf`
- `genpass`
- `genuser`
- `gui`
- `silent`

The `genconf` command generates a config file with default values filled out. This is also done automatically when you invocate `silent` and `gui`, but you can call this command to get that generated before you try to use the other commands.

The `genpass` command generates a password with your given specifications.

The `genuser` command generates a username using a random listing in the `usernames` array in the JSON configuration file.

The `gui` command opens up a wxpython gui that will allow you to create a temporary user. It includes the `genpass` and `genuser` functionality.

The `silent` command does the same thing as `gui`, but it does so without a gui and the `genpass` and `genuser` commands (it is expected that you use those commands before using the silent command).

As an aside, the gui was constructed using WxFormBuilder. When editing that file and generating code, you need to comment out `import wx.xrc` in `wxfbgui.py` as that seems to report memory leaks (even though there are not any).

## Usage

In order to get an up-to-date version of the arguments that are passed in to the module, all you have to do is type:
```
#!bash

python {genpass, genuser, gui, silent} --help
```
And you should get a listing of the different arguments that can be passed in.

### genconf

```
#!bash

python addtmpusr genconf [-h, --help] [-v, --verbose]
```

| Argument          | Description                                                                                  | Required? | Default  |
| ----------------- | -------------------------------------------------------------------------------------------- | --------- | -------- |
| `-h`, `--help`    | Show help message and exit                                                                   | No        | N/A      |
| `-v, --verbose`   | Whether or not to log verbose messages and datetime stamps. Errors will still be logged.     | No        | N/A      |


### genpass

```
#!bash

python addtmpusr genpass [-h, --help] [-v, --verbose] [-l LOWER] [-u UPPER] [-d DIGIT] [-s SYMBOL]
```

| Argument          | Description                                                                                  | Required? | Default  |
| ----------------- | -------------------------------------------------------------------------------------------- | --------- | -------- |
| `-h`, `--help`    | Show help message and exit                                                                   | No        | N/A      |
| `-l, --lower`     | The number of lowercase alpha characters                                                     | No        | 3        |
| `-u, --upper`     | The number of uppercase alpha characters                                                     | No        | 3        |
| `-d, --digit`     | The number of digits                                                                         | No        | 3        |
| `-s, --symbol`    | The number of symbols                                                                        | No        | 1        |
| `-v, --verbose`   | Whether or not to log verbose messages and datetime stamps. Errors will still be logged.     | No        | N/A      |


### genuser

```
#!bash

python addtmpusr genuser [-h, --help] [-v, --verbose] [-p PREFIX]
```

| Argument          | Description                                                                                  | Required? | Default  |
| ----------------- | -------------------------------------------------------------------------------------------- | --------- | -------- |
| `-h`, `--help`    | Show help message and exit                                                                   | No        | N/A      |
| `-p, --prefix`    | The prefix before the username generated.                                                    | No        | tmp_     |
| `-v, --verbose`   | Whether or not to log verbose messages and datetime stamps. Errors will still be logged.     | No        | N/A      |


### gui

```
#!bash

python addtmpusr gui [-h, --help]
```


### silent

```
#!bash

python addtmpusr silent [-h, --help] [-v, --verbose] -u USERNAME -g GROUP -x EXPDATE -p PASS -n NOTES
```

| Argument          | Description                                                                                  | Required? | Default     |
| ----------------- | -------------------------------------------------------------------------------------------- | --------- | ----------- |
| `-h`, `--help`    | Show help message and exit                                                                   | No        | N/A         |
| `-u, --username`  | The username of the new user. It must be unique.                                             | Yes       | N/A         |
| `-g, --groups`    | A comma-delimited list of groups that will be added to the user.                             | No        | (see below) |
| `-x, -expdate`    | The expiration date of the account when it will be disabled. It must be in the future.       | Yes       | N/A         |
| `-n, --notes`     | The notes associated with the temporary user.                                                | Yes       | N/A         |
| `-v, --verbose`   | Whether or not to log verbose messages and datetime stamps. Errors will still be logged.     | No        | N/A         |

*DEFAULT FOR GROUPS*: "int-ext-sftp,ext-sftp". For groups, you can only choose from these three choices:

- "ext-sftp"
- "int-ext-sftp"
- "int-ext-sfp,ext-sftp"

*NOTE ON NOTES*: This is not stored in `/etc/passwd`. Rather, it is stored in a JSON file outputted to `tmp_users/{username}.json`.

## Config File

The config file is meant to be used as a way to ensure that you are creating temporary users consistently. If it does not already exist, when you open the program via `gui`, try to create a user via `silent`, or manually generate the config file with `genconf`, it will create the file in `path-to-addtmpusr/config/addtmpusr.json` and populate it with the following default values:

```
#!json
{
    "useroutput": "path-to-addtmpusr/config/tmp_users", 
    "shell": "/sbin/nologin", 
    "umask": "022"
}
```

When using the `genuser` functionality (either with the command itself or via the WxPython gui), you must have an array of potential usernames in this file like so:

```
#!json
{
    "useroutput": "path-to-addtmpusr/config/tmp_users", 
    "shell": "/sbin/nologin", 
    "umask": "022",
    "usernames": [
        "apple", 
        "apricot", 
        "avocado", 
        "banana"
    ], 
}
```


-------------------------------------------------------------------------------

** Script Author:** Angela Gross