import os, json

class AddTmpUsrAppConfig(object):
    """A singleton class that manages a standardized configuration directory.

    Attributes:
        ConfigDir (str): The config directory path.
        ConfigFile (str): The config file path.
        data (dict): The JSON object representing the ConfigFile.
    """
    _instance = None
    _appName = 'addtmpusr'
    def __init__(self):
        super(AddTmpUsrAppConfig, self).__init__()

        # create base dir if it doesnt exist
        if not os.path.exists(self.ConfigDir):
            os.mkdir(self.ConfigDir)

        # get config file
        self.data = dict()
        if os.path.exists(self.ConfigFile):
            with open(self.ConfigFile) as dataFile:
                self.data = json.load(dataFile)

    """Singleton accessor method to ensure that the application uses a shared instance of user configuration data.

    Returns:
        AddTmpUsrAppConfig: The singleton instance.
    """
    @staticmethod
    def Instance():
        if AddTmpUsrAppConfig._instance is None:
            AddTmpUsrAppConfig._instance = AddTmpUsrAppConfig()
        return AddTmpUsrAppConfig._instance

    """Get the config directory path.

    Returns:
        str: The config directory path.
    """
    @property
    def ConfigDir(self):
        currDir = os.path.dirname(os.path.realpath(__file__))
        return os.path.join(currDir, "config")

    """Get the config file path.

        Returns:
            str: The config file path.
    """
    @property
    def ConfigFile(self):
        appName = AddTmpUsrAppConfig._appName
        return os.path.join(self.ConfigDir, "{appName}.json".format(appName=appName))

    """Gets the path relative to the user config directory.

        Params:
            relPath (str): The relative path to the user config directory.

        Returns:
            str: The path relative to the user config directory.
    """
    def GetConfigFile(self, relPath):
        return os.path.join(self.ConfigDir, relPath)

    """Whether or not the path relative to the user config directory exists.

        Params:
            relPath (str): The relative path to the user config directory.

        Returns:
            bool: Whether or not it exists.
    """
    def ConfigFileExists(self, relPath):
        path = self.GetConfigFile(relPath)
        return os.path.exists(path)

    """Retrieve a value from the user config data dict.

        Params:
            key (str): The dict key
            default (str): The default value if it does not exist.

        Returns:
            str: The value from the user config.
    """
    def GetConfigValue(self, key, default=None):
        return self.data.get(key, default)

    """Writes a value to the user config data dict.

        Params:
            key (str): The dict key
            value (str): The value to write
    """
    def WriteConfigValue(self, key, value):
        self.data[key] = value

    """Writes the user config data dict to the file.
    """
    def SaveConfig(self):
        with open(self.ConfigFile, 'w') as configFile:
            json.dump(self.data, configFile, indent=4)
