#!/usr/local/lib/anaconda/envs/user-management/bin/python

""" addtmpusr

This python program aides in the management of temporary users. Although these temporary users can have many purposes, the main purpose for 
us is to limit the window in which external users can access our FTP filesystem, `/ftp/ext`, via SFTP.

There are five commands that can be used with this progam:

- `genconf`
- `genpass`
- `genuser`
- `gui`
- `silent`

The `genpass` command generates a password with your given specifications.

The `genuser` command generates a username using a random listing in the `usernames` array in the JSON configuration file.

The `gui` command opens up a wxpython gui that will allow you to create a temporary user. It includes the `genpass` and `genuser` 
functionality.

The `silent` command does the same thing as `gui`, but silently and without the `genpass` and `genuser` commands (it is expected that you 
use those commands before using the silent command).
"""

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

import gui, arguments, console
from config import AddTmpUsrAppConfig as conf

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

# ---------------------------------------------------------------------------------------------------------------------------------------
# MAIN METHOD
# ---------------------------------------------------------------------------------------------------------------------------------------

def Main():
    """Parses arguments and acts depending the command called. Saves the config file before closing.
    """

    argParser = arguments.Arguments()
    if argParser.args['command'] == 'genpass':
        genpass = console.GenPassConsole(argParser.args)
        genpass.CreatePass()

    elif argParser.args['command'] == 'genuser':
        genuser = console.GenUserConsole(argParser.args)
        genuser.CreateUser()

    elif argParser.args['command'] == 'gui':
        app = gui.AddTmpUsrApp(False)
        app.MainLoop()

    elif argParser.args['command'] == 'silent':
        silentConsole = console.SilentConsole(argParser.args)
        silentConsole.CreateUser()

    elif argParser.args['command'] == 'genconf':
        genconf = console.GenConfConsole(argParser.args)
        genconf.CreateConf()

    conf.Instance().SaveConfig()

if __name__ == "__main__":
    Main()

# ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
